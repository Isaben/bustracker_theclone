package com.example.isaben.funciona;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by abner on 02/07/2016.
 */
public class Route {
    private int id;
    private String name;
    private String description;
    private int[] id_buses;
    private ArrayList<LatLng> path;

    public Route(int id, String name, String description, ArrayList<LatLng> path){
        this.id = id;
        this.name = name;
        this.description = description;
        //this.id_buses = id_buses;
        this.path = path;
    }

    public ArrayList<LatLng> getPath(){
        return this.path;
    }

}
