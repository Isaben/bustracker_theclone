package com.example.isaben.funciona;


        import android.content.Context;
        import android.widget.Toast;

        import com.android.volley.Request;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.JsonArrayRequest;
        import com.android.volley.toolbox.StringRequest;
        import com.android.volley.toolbox.Volley;
        import com.google.android.gms.maps.model.LatLng;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;


public class ConnectionManager {

    private final String hostPrefix;
    private JSONArray json;
    private Context context;
    private RequestQueue requestQueue;

    public ConnectionManager(Context context, String hostPrefix){
        this.hostPrefix = hostPrefix;
        this.context = context.getApplicationContext();
        requestQueue = Volley.newRequestQueue(this.context);
    }

    private ArrayList<Route> parseRoutesList(JSONArray routesList){
        ArrayList<Route> routes = new ArrayList<>(0);
        try{
            for(int i = 0; i < routesList.length(); i++){
                JSONObject ob = routesList.getJSONObject(i);
                routes.add(parseRoute(ob));
            }
        } catch (Exception e){
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
        }
        return routes;
    }

    public Route parseRoute(JSONObject ob){

        try{
            int id = ob.getInt("id_routes");
            String name = ob.getString("name");
            String description = ob.getString("description");
            ArrayList<LatLng> path = parseRoutePath(ob.getJSONArray("points"));
            return new Route(id, name, description, path);

        }
        catch (Exception e){
            Toast.makeText(context,"eita caralho"+e.getMessage(),Toast.LENGTH_LONG).show();
            return null;
        }

    }

    private ArrayList<LatLng> parseRoutePath(JSONArray points){
        ArrayList<LatLng> path = new ArrayList<>(0);
        try {
            for (int i = 0; i < points.length(); i++) {
                JSONObject point = points.getJSONObject(i);
                Double latitude = point.getDouble("latitude");
                Double longitude = point.getDouble("longitude");
                path.add(new LatLng(latitude, longitude));
            }
        } catch (JSONException e){
            Toast.makeText(context,"eita doido"+ e.getMessage(),Toast.LENGTH_LONG).show();
        }
        return path;
    }

    public void setQueue(StringRequest request){
        this.requestQueue.add(request);
    }
}

