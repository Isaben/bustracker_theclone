package com.example.isaben.funciona;



        import android.util.Log;
        import android.widget.Toast;

        import java.text.DateFormat;
        import java.text.ParseException;
        import java.text.SimpleDateFormat;
        import java.util.Date;

/**
 * Created by abner on 02/07/2016.
 */

public class Bus {
    private int id;
    private double latitude;
    private double longitude;
    private Date last_update;

    public Bus(int id, double latitude, double longitude, String date){
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            this.last_update = df.parse(date);
        }
        catch (ParseException pe){
            Log.println(Log.ERROR, "MapActivity", pe.getMessage());
        }
    }
}
