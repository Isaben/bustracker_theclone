package com.example.isaben.funciona;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public Handler handler = new Handler();
    public static final String URL ="http://santana.azurewebsites.net/BusTrackerAPI/index.php/routes/86";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        handler.postDelayed(rode, 0);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // marcador no início da rota
        LatLng sydney = new LatLng(-3.692908, -40.357468);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Feijoada"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


    }


    public void putRoute(Route route){
        PolylineOptions line = new PolylineOptions();
        line.geodesic(true);
        ArrayList<LatLng> rota = route.getPath();
        for(int i = 0; i < rota.size(); i ++){
            line.add(rota.get(i));
        }

        mMap.addPolyline(line);
    }

    public void sendRequest(){
        final ConnectionManager conexao = new ConnectionManager(this, URL);

        StringRequest stringRequest = new StringRequest( URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject ob = new JSONObject(response);
                            Route rota = conexao.parseRoute(ob);
                            putRoute(rota);

                        }
                        catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("yolo", "nigga");
                    }
                });

        conexao.setQueue(stringRequest);

    }

    Runnable rode = new Runnable(){
        @Override
        public void run(){
            sendRequest();
            Log.e("ta mandando", "manda meu fi");
            handler.postDelayed(rode, 3000);
        }

    };

}
